#!/usr/bin/env python

try:
    from http.server import SimpleHTTPRequestHandler
    import socketserver
except:
    from SimpleHTTPServer import SimpleHTTPRequestHandler
    import SocketServer as socketserver
import sys

PORT = 8000 if len(sys.argv) <= 1 else int(sys.argv[1])
HOST = "" if len(sys.argv) <= 2 else sys.argv[2]

class Server(socketserver.TCPServer):
    def __init__(self, addr, handler):
        self.allow_reuse_address = True
        if ':' in addr[0]:
            self.address_family = socket.AF_INET6
        super().__init__(addr, handler)

class Handler(SimpleHTTPRequestHandler):
    def send_response(self, code, message=None):
        # Grr, cannot use this in Python 2 as SimpleHTTPRequestHandler is not a
        # new-style object.
        # SimpleHTTPRequestHandler is not a object...
        SimpleHTTPRequestHandler.send_response(self, code, message)
        self.send_header('Cache-Control', 'no-cache')

httpd = Server((HOST, PORT), Handler)

print("Serving at port %d" % PORT)
httpd.serve_forever()
