/* globals requirejs */
requirejs.config({
    paths: {
        'd3': '../lib/d3'
    },
    baseUrl: 'js'
});
requirejs(['main']);
