define(['assert'], function(assert) {
    'use strict';
    // types: bc (bachelor college), major (default), bep
    // (assumption: every course is 5 ECTS and one must do 15 ECTS in a
    // quartile, filling it up with Electives)
    // for new courses without code, use "DS..."
    var courses = [
        // Bachelor college core courses
        { "id": "2WAB0", "name": "Calculus",   "type": "bc", "when": "Y1Q1" },
        { "id": "3NAB0", "name": "Physics",    "type": "bc", "when": "Y1Q2" },
        { "id": "0LEB0", "name": "Modelling",  "type": "bc", "when": "Y1Q3" },
        { "id": "0SAB0", "name": "USE Basics", "type": "bc", "when": "Y1Q4" },
        { "id": "7NXB0", "name": "Design",     "type": "bc", "when": "Y2Q1" },
        // Data Science basic courses
        { "id": "2IP90", "name": "Programming",             "when": "Y1Q1" },
        { "id": "DS001", "name": "Introduction to Data Science",    "when": "Y1Q1" },
        { "id": "2IT60", "name": "Logic and Set Theory",    "when": "Y1Q2" },
        { "id": "2IO80", "name": "DBL Hypermedia",          "when": "Y1Q3" },
        { "id": "2IC60", "name": "Computer networks and security",  "when": "Y1Q4" },
        { "id": "2ID40", "name": "Human Technology Interaction",    "when": "Y1Q4" },
        { "id": "2WF20", "name": "Linear Algebra",          "when": "Y2Q1" },
        { "id": "2IV60", "name": "Computer Graphics",       "when": "Y2Q3" },
        // Advanced courses
        { "id": "2ID50", "name": "Datamodelling and Databases", "when": "Y2Q2" },
        { "id": "2ID60", "name": "Web Technology",              "when": "Y2Q2" },
        { "id": "DS002", "name": "Workflow",                    "when": "Y2Q3" },
        { "id": "2IL50", "name": "Data Structures",             "when": "Y2Q3" },
        { "id": "2IOC0", "name": "DBL Information Systems",     "when": "Y2Q4" },
        { "id": "2DI90", "name": "Probability and Statistics",  "when": "Y2Q4" },
        { "id": "DS003", "name": "Data Visualization",          "when": "Y3Q1" },
        { "id": "2IIC0", "name": "Business Information Systems","when": "Y3Q1" },
        { "id": "2IID0", "name": "Web Analytics",               "when": "Y3Q2" },
        { "id": "2IO90", "name": "DBL Algorithms",              "when": "Y3Q2" },
        { "id": "DS004", "name": "Bachelor End Project", "type": "bep", "when": "Y3Q3+Y3Q4" },
    ];

    var descriptions = {
        "2WAB0": "Technical science uses mathematics as a language to describe phenomena and to solve problems. With trigonometry one can describe waves. In many problems an optimal setting of parameters should be found. This can be done with mathematical techniques such as differentiation. Integral calculus is needed for the calculation of energy and power. Many models use differential equations. In this course you will learn these and other basic techniques but also the formal aspects of mathematics. This course is given in an A and B version, with A being more numerical over B that focusses more on a symbolical approach to calculus.",
        "3NAB0": "This course is available in an A and B version and will give future engineers the conceptual basis of a few fundamental concepts of physics, with which they can form a “founded” idea of both scientific and technological developments in the fields of medical, nano and energy technology, climate control etc. The importance of the concepts that will be treated, will be cleared up using engaging demonstrations and examples in which the above named technologies are important.",
        "0LEB0": "Engineers solve problems, and use model to do so. This always requires a translation to rephrase the initial problem in mathematical terms.. A second translation comes in to interpret the mathematical result in the context of the initial problem. This course introduces methods and techniques to perform such translations in a structured manner.",
        "0SAB0": "How to build your technological dream, and avoid a nightmare? Engineers develop technology and thereby shape user experiences, enterprises, and society. USE basis teaches you how to analyze the historically developed relationships between technology and users (U), society (S), and enterprise (E) (in short: USE), and to explore its ethical implications for engineering practice. Finally you will apply these insights in a technological vision that takes relevant USE issues into account.",
        "7NXB0": "Anyone can learn how to design. The basic Design course consists of doing a design project, supported by (web) lectures, weekly assignments, workshops and coaching. The scope of all design projects is determined by a central theme that allows student teams to specify their own design challenge or problem, their own design and solution space, and their own design process. Solutions for the design problem or opportunity require multidisciplinary input.",
        // Data Science basic courses
        "2IP90": " Course on hands-on Java programming, basic principles of object-oriented programming, and the Swing framework for graphics and user interaction.",
        "DS001": "Are you interested in understanding how data works, what we can do with it and how to do this? This course introduces you in the fascinating world of data. You will learn about data science basics. This will improve your data science knowledge and will give your career as a data scientist a kickstart!",
        "2IT60": "Logical reasoning is an indispensable tool when designing a solution to any complex technical problem. This course discusses the principles of correct logical reasoning. You learn to formulate statements in unambiguous logical language, and to manipulate statements in a structured and logically valid manner. At the end of the course you are able to give simple mathematical proofs, in particular using the technique of mathematical induction.",
        "2IO80": "In this course students will learn: - about the concepts of hypermedia (in a Web context), - how to use wiki tools for creating own wiki sites, - how to work efficiently in a group.",
        "2IC60": "Interactivity is key for modern ICT systems. A computer network is an infrastructure of interconnected devices and a facilitator of distributed applications. Yet interactivity comes with security threats. This course covers the organization of computer networks (e.g. the Internet), basics of security and main network and security protocols. Students learn to explain solutions to standard problems and to analyze simple protocols with respect to correctness, performance, reliability and security.",
        "2ID40": "In Human-Technology Interaction we learn how to design and implement usable and user-friendly interfaces for all kinds of devices that involve technology. This includes consumer electronics, graphical computer interfaces and mobile applications. Together we can make the world a better place, with interfaces that do not require studying a complex manual but that are intuitive and easy to use. We practice the design, implementation and evaluation of our interfaces in group projects.",
        "2WF20": "Systems of linear equations, matrices and vector spaces are the core topics in this course. Vector spaces consist of vectors (just think of an arrow with a certain direction and a certain length). By introducing abstract vector spaces, the corresponding theory can be applied in various settings. This course is indispensable for most branches of mathematics, and even for many of the engineering sciences.",
        "2IV60": "Computer graphics is the most visible part of computer science. Games, interactive applications, movies, the web: automatically generated graphical images are everywhere. In the course, concepts of 2D and 3D computer graphics are explained, and in the end students will be able to design and develop an interactive graphical application, based on OpenGL.",
        "2ID50": "Our lives are awash in data (e.g., social, business, web) which only continues to grow in both quantity and variety. Database management systems are the key technologies which facilitate our practical use of these massive data sets. In this course, we study fundamental concepts, such as data model design and formulation of queries against databases, which underpin the effective practical use of industrial strength data management systems.",
        "2ID60": "The course provides the student with knowledge of and insight into the rapidly evolving field of web technology by means of an overview of a number of relevant topics. The focus is on hands-on experience with a wide variety of these technologies. The students will be required to carry out an assignment for which they will use representatives of technologies discussed in the lectures for the realization of a relevant application.",
        "DS002": "This course introduces the basic concepts of workflow management. The emphasis is on modeling workflow processes and the characteristics of contemporary workflow management. Workflow processes are a specific type of operational processes typically associated with work processes in administrative environments. However, any case-driven operational process falls in this category. Workflow technology provides the functionality to support these processes. Since this technology is adopted in many enterprise information systems knowledge about these systems and experience in making and enacting workflow models is relevant for students in operations management.",

        // Advanced courses
        "2IL50": "For solving algorithmic problems many aspects need to be mastered: efficient ways of storing and manipulating (large amounts of ) data, algorithm design techniques, how to establish that an algorithm is correct, and how to analyze the efficiency of an algorithm.",
        "2IOC0": "Information systems are the central software of today’s information driven economy. Their design and improvement is vital to the success of an organization like a company or a public administration. Modern information system are typically “process aware” meaning that the order in which information is processed or shown to a user is expressed in models. However, the actual challenge for the success of such a system lies in two other areas: it has to serve its users the necessary information in the right form to achieve a particular task, and it has to access and show this information within a technical environment that one cannot influence. In this course, we focus on the aspect of designing information retrieval and information visualization within a given technical environment and user context. We use techniques from “design thinking” to analyze the problems of an existing information system and designing solutions. Their implementation requires integrating an existing process engine with a database and a web-based frontend through programming. At the end of the course, students can experience the effectiveness of their designs in a contest for processing information.",
        "2DI90": "This course gives an introduction to probability and statistics. In many situations it is impossible to predict how a complex system will behave. Models capturing this uncertainty are essential to study such systems. Probability theory gives us the tools to do this in a powerful manner. Statistics, on the other hand, concerns what can be learned from data. In this course you will learn how to build probabilistic models, perform statistical analyses of real data, and the theory behind all this.",
        "DS003": "You will learn about different visualization forms and techniques, and directly apply this in preparing a set of data for visualization with the right tool. Next, you will learn how to find the best pieces or aspects of a data set and then how choose the right type of visualization to make a strong point about the data. ",
        "2IIC0": "Process-aware information systems are information systems that are configured on the basis of process models. In some systems the process models are explicit and can be adapted while in other systems they are implicit. It is clear that in any enterprise, business processes and information systems are strongly intertwined. In this course, we study the fundamental relationship between systems and processes and we model complex systems involving processes, humans, and organizations.",
        "2IID0": "In Web Analytics course we focus on the utility of different technologies for understanding the Web as a whole, for modeling the behavior of the individual Web users and user groups, for using Web as an experimentation platform. We will cover the topics of OLAP style data exploration, data mining, and social network analysis. We will give a special attention to understanding search, recommendations and advertisements on the Web.",
        "2IO90": "How can we automatically put labels (like city names) on a map in such that the labels do not overlap? How can we compute train schedules that optimally use the available tracks and trains? How can we reconstruct 3D objects from laser scans? These are just a few examples of challenging problems that require efficient algorithmic solutions and effective software implementations of those solutions. In this course you will attack such an algorithmic problem in a team of five or six students.",
        "DS004": "In the end of your bachelor you have to do a final project. The choices in the project are mostly up to you. You choose whether to work on your own or in pairs. You choose what area in data science you cover in your project. And you choose the kind of project."

    };

    function getQuartile(when) {
        var y_q = /^Y([1-3])Q([1-4])$/.exec(when);
        assert(y_q, "Invalid when: " + when);
        var y_i = y_q[1] - 1, q_i = y_q[2] - 1;
        return schedule[y_i][q_i];
    }

    var coursesById = {};

    // 2d array from years to quartiles to courses
    var schedule = [];
    // make an empty schedule
    var years = 3, quartiles = 4;
    for (var i = 0; i < years; i++) {
        schedule[i] = [];
        for (var j = 0; j < quartiles; j++) {
            schedule[i][j] = [];
        }
    }

    // find all courses by ID and fill the schedule
    courses.forEach(function(course) {
        assert (!(course.id in coursesById),
            "Course is already known: " + course.id);
        coursesById[course.id] = course;
        course.when.split('+').forEach(function(when) {
            getQuartile(when).push(course);
        });

        // link description too.
        course.description = descriptions[course.id];
    });

    // fill in the remaining time slots
    schedule.forEach(function(year, year_i) {
        year.forEach(function(quartile, quartile_i) {
            while (quartile.length < 3) {
                quartile.push({
                    "id": "",
                    "name": "Elective / USE",
                    "type": "elective",
                    "when": "Y" + (year_i + 1) + "Q" + (quartile_i + 1)
                });
            }
        });
    });

    return {
        "courses": courses,
        "schedule": schedule
    };
});
/*
Packages:
Programming - Linear Algebra - Computer Graphics - Data Visualisation
Programming - Human Technology Interaction - Web Technology - Web Analytics
Programming - Data Structures - DBL Algorithms

Electives and USE:
Year 1 - 2x Elective
Year 2 - 3x Elective/USE
Year 3 - 6x Elective/USE
*/
