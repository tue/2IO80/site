define(function() {
    'use strict';
    var buildings = [
        {
            "name": "Students Sports Centre",
            "description": "Have you ever heard about bossaball, underwater hockey, canoepolo or kempo jitsu? Besides all educational optionalities on the campus, there’s also the <a href=\"\http://venus.tue.nl/sci-cgi/index.opl\"\>Student Sports Centre</a> (SSC) which offers an extensive variety of sports for you to try out and practice, all in order to keep the TU/e-students wholesome. With 70 kinds of sports, special courses, a broad range of facilities, doors that are open 7 days per week, qualified sports instructors, 38 student sports associations and over 11000 members, the SSC is among the largest student collectives in Eindhoven.",
            "coords": [
                [355,134],
                [355,221],
                [648,221],
                [648,134]
            ]
        }, {
            "name" : "Zwarte doos",
            "description": "The cinema “De Zwarte Doos” has a broad range of movies mostly selected by students, so there will always be a movie for you. Even if you don’t like movies, you can drink a beer in the cafeteria as well.",
            "coords": [
                [297,647],
                [321,647],
                [321,666],
                [304,666],
                [304,659],
                [297,659],
            ]
        }, {
            "name": "Metaforum",
            "description": "The Metaforum is the place on the campus where you can work on your assignments or do your homework. There are multiple facilities located in the metaforum which are present to facilitate your study at the TU/e. The library is located there with a broad selection of literature to get you going on your assignments. If you want something to be printed, then this is also possible in the printshop of the Metaforum. Whenever something is wrong with your <a href=\"#/studying/Laptops\"\>notebook</a> or there is something that you want explained about your notebook and its uses then the laptop service centre in the Metaforum is your place to be.\nThe Metaforum is built in such a way that both the students that want to work silently and the students that have to do group assignments can work in the same building. It is designed in such a way that the building consists of three layers while the lower layers will not be bothered with the sound that’s produced on the top layer. So every student, no matter what his intention at the Metaforum is, will have a place to work effectively on his or her study.",
            "coords": [
                [453,492],
                [512,492],
                [512,607],
                [453,607],
            ]
        }, {
            "name": "Auditorium",
            "description": "The Auditorium is the lecture center of the TU/e. It contains eight big lecture halls with up to 300 seats. It has eight small lecture halls with at maximum 89 seats as well. Besides, the building has a canteen where you can buy your lunch. After you have bought your delicacy or bread with cheese, you can lunch with your fellow students in a big lunch hall. The lunch hall is used for events as well, such as business markets and orientation markets to future students.",
            "coords": [
                [278,496],
                [278,528],
                [281,528],
                [281,540],
                [343,540],
                [343,531],
                [345,531],
                [345,518],
                [348,518],
                [348,513],
                [345,513],
                [345,507],
                [343,507],
                [343,496],
                [331,496],
                [331,488],
                [329,488],
                [329,496],
            ]
        }, {
            "name": "Main Building",
            "description": "The main building of the TU/e is under construction at the moment. In 2018 it will open again. After the renovation the building will be more sustainable and will have a technological visual appeal. Mostly it will be used by test labs, but it will also contain two faculties of the TU/e.",
            "coords": [
                [400,454],
                [400,482],
                [387,482],
                [387,510],
                [397,510],
                [397,528],
                [391,528],
                [391,558],
                [401,558],
                [401,587],
                [416,587],
                [416,558],
                [429,558],
                [429,528],
                [419,528],
                [419,519],
                [429,519],
                [430,482],
                [416,482],
                [416,454],
            ]
        }, {
            "name" : "Bus Stops",
            "description": "Students and staff can take a bus to travel to and from TU/e. The bus makes its route four times an hour during peak hours (7-9 a.m. and 4-6 p.m.) and has four stops on campus.\nThe bus carries the number 104 and will be bound for ‘TU/e Science Park’ officially. The 104 makes four stops along its route: Laplace Square (Laplace building), Lismortel (teacher training building), Rondom East (Differ building), and Rondom South (Flux building). The bus route is part of the TU/e mobility plan as one of the measures to discourage car use, promote public transportation, and make buildings easier to reach.",
            "coords": [

            ]
        }, {
            "name" : "Parking",
            "description": "12 parking locations can be found all around the campus, but please do keep in mind that parking on the campus isn’t free, the TU-website can be consulted for the parking rates. You can also park on the Prof. dr. Dorgelolaan, which is nearby the TU/e-campus. The parking fee there is €3,70 per day.",
            "coords": [
            ]
        }, {
            "name" : "Supermarket",
            "description": "The supermarket, located in the FLUX building, is a new place on the TU/e campus. Most students will have to take care of their own food, so this Spar supermarket can get quite useful if you don’t want to do some grocery shopping at your own place. But if you just want a little snack or something else to eat or to drink while you are on the campus, the supermarket will take care of these needs as well.",
            "coords": [
                [685,538],
                [720,538],
                [720,514],
                [685,514],
            ]
        }, {
            "name" : "Bike Repair Shop",
            "description": "Once you’ve transformed into a real student, cars will be too expensive and you’ll certainly be too lazy to walk. The perfect solution is accepting the bike as your main mean of transport if you live in Eindhoven. Bikes however need to be repaired now and then. Fortunately there is a bike repair shop on the campus (“de groene fietser”). Every monday through thursday you can bring your bike from 8 AM to 5:30 PM, and every friday from 8 AM to 2 PM.",
            "coords": [
                [694,254],
                [694,265],
                [709,265],
                [709,254],
            ]
        }
    ];
    buildings.forEach(function(building) {
        building.id = building.name.toLowerCase().replace(/ /g, '-');
    });
    return buildings;
});
