define(function() {
    'use strict';

    function assert(cond, message) {
        if (!(cond)) {
            throw message;
        }
    }

    return assert;
});
