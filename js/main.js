define(function() {
    'use strict';

    var all_pages = 'home studying curriculum campus career disclaimer contact sitemap'.split(' ');

    var cacheBuster = '';
    if (/dev/.test(location.search)) {
        cacheBuster = '?t=' + (new Date()).getTime();
    }

    /* loads a page into view and execute scripts */
    var orig_title = document.title;
    function loadContent(name, text) {
        var contentElement = document.getElementById('content');
        contentElement.innerHTML = text;
        document.body.dataset.page = name;

        // change title
        var h1s = contentElement.getElementsByTagName('h1');
        if (h1s.length) {
            document.title = h1s[0].textContent + ' - ' + orig_title;
        } else {
            document.title = orig_title;
        }

        // execute scripts in page
        [].forEach.call(contentElement.getElementsByTagName('script'),
                function(origScript) {
            var script = document.createElement('script');
            // copy attributes
            [].forEach.call(origScript.attributes, function(a) {
                script.setAttribute(a.name, a.value);
            });
            // copy contents
            script.textContent = origScript.textContent;
            origScript.parentNode.replaceChild(script, origScript);
        });
    }

    /* tries to navigate to a page */
    var current_page;
    function selectPage(page, anchor) {
        if (all_pages.indexOf(page) == -1) {
            // TODO: 404
            console.log("404 " + page);
            return false;
        }

        if (current_page === page) {
            return;
        }
        current_page = page;
        // move to top
        scrollTo(0, 0);

        console.log("Loading " + page);
        var http = new XMLHttpRequest();
        http.onload = function() {
            loadContent(page, http.responseText);

            // focus element when loaded
            var anchor_id = extractHash(anchor);
            if (anchor_id) {
                var selection = document.getElementById(anchor_id);
                if (selection) {
                    selection.scrollIntoView();
                }
            }
        };
        http.open('get', 'pages/' + page + '.html' + cacheBuster);
        http.send(null);
        return true;
    }

    function extractHash(url) {
        var m = /#(.+)/.exec(url);
        return m ? m[1] : '';
    }
    function extractPageComponents(url) {
        var m = /^\/([^\/]+)(.*)$/.exec(extractHash(url));
        return m ? [m[1], m[2]] : ['', ''];
    }

    function changePageUrl(url) {
        var m = extractPageComponents(url);
        var hash = m.join('');
        if (hash) {
            selectPage(m[0], hash);
        } else {
            location.hash = '#/home';
            selectPage('home', hash);
        }
    }

    addEventListener('hashchange', function(ev) {
        var anchor = extractHash(ev.newURL);
        // magic key to jump to the top of the page.
        if (anchor === 'top') {
            var pageURL = '#/' + extractPageComponents(ev.oldURL)[0];
            scrollTo(0, 0);
            location.replace(pageURL);
            return;
        }

        changePageUrl(ev.newURL);
    });

    // Tries to load the current page, falling back to "home" for unknown URLs.
    changePageUrl(location.hash);
});
