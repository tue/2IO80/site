define(['courses', 'campus-coords'], function(courses, campus_coords) {
    'use strict';

    function Link(name, url) {
        this.name = name;
        this.url = '#/' + url;
        this.children = [];
    }

    var add = function(parent, name, url) {
        var link = new Link(name, url);
        parent.push(link);
        return link.children;
    };
    var links = [];
    add(links, 'Home', 'home');
    add(links, 'Studying', 'studying');
    var course_links = add(links, 'Curriculum', 'curriculum');
    courses.courses.forEach(function(course) {
        add(course_links, course.name, 'curriculum/' + course.id);
    });

    var campus_links = add(links, 'Campus', 'campus');
    campus_coords.forEach(function(building) {
        add(campus_links, building.name, 'campus/' + building.id);
    });

    add(links, 'Career', 'career');
    add(links, 'Contact', 'contact');
    add(links, 'Disclaimer', 'disclaimer');
    add(links, 'Sitemap', 'sitemap');

    function make_list(parent, item) {
        if (item instanceof Link) {
            var li = document.createElement('li');
            var link = document.createElement('a');
            link.href = item.url;
            link.textContent = item.name;
            li.appendChild(link);

            // child pages
            if (item.children.length) {
                make_list(li, item.children);
            }

            parent.appendChild(li);
        } else {
            var list = document.createElement('ul');
            item.forEach(function(item) {
                make_list(list, item);
            });
            parent.appendChild(list);
        }
    }

    return function() {
        var container = document.getElementById('sitemap-content');

        make_list(container, links);
    };
});
