define(['campus-coords', 'd3'], function(coords, d3) {
    'use strict';

    var imageUrl = 'images/campus.jpg';
    var width = 1280, height = 893;
    var scalingFactor = 780 / width;

    function initSvg(svg) {
        svg
            .attr('width', width * scalingFactor)
            .attr('height', height * scalingFactor);
        // set the container size to the same size as svg element
        d3.select(svg.node().parentNode)
            .style('width', svg.attr('width') + 'px')
            .style('height', svg.attr('height') + 'px');
        // show the full image (one could apply clipping here)
        svg
            .attr('viewBox', [
                0,
                0,
                width,
                height,
            ].join(' '));

        svg.append('image')
            .attr('xlink:href', imageUrl)
            .attr('width', width)
            .attr('height', height);

        svg.append('g')
            .selectAll('polygon')
                .data(coords)
            .enter()
                .append('a')
                    .attr('xlink:href', function(d) {
                        return '#/campus/' + d.id;
                    })
                .append('polygon')
                    .attr('class', 'building')
                    .attr('points', function(d) {
                        return d.coords.join(' ');
                    })
                    .append('title')
                        .text(function(d) {
                            return d.name;
                        });
    }

    function try_add_image(building, element) {
        var image_url = 'images/campus-' + building.id + '.png';
        var img = new Image();
        img.src = image_url;
        img.className = 'description-picture';
        img.addEventListener('load', function() {
            var clear = document.createElement('div');
            clear.style.clear = 'both';

            if (img.width > 500) {
                img.classList.add('large');
                element.appendChild(img);
                element.appendChild(clear);
            } else {
                img.classList.add('small');
                element.insertBefore(img, element.firstChild);
                element.insertBefore(clear, element.firstChild);
            }
        });
    }

    return function() {
        var svg = d3.select('#campus-map').append('svg');
        initSvg(svg);

        var citems = document.getElementById('campus-items');
        coords.forEach(function(building) {
            var header = document.createElement('h2');
            header.id = '/campus/' + building.id;
            header.textContent = building.name;
            citems.appendChild(header);

            var text_block = document.createElement('p');
            text_block.innerHTML = building.description;
            citems.appendChild(text_block);

            var links_block = document.createElement('p');
            var back_to_top = document.createElement('a');
            back_to_top.href = '#top';
            back_to_top.textContent = 'Back to top';
            links_block.appendChild(back_to_top);
            citems.appendChild(links_block);

            try_add_image(building, text_block);
        });
    };
});
