define(['courses'], function(courses) {
    'use strict';

    function makeCoursesList(courseCallback) {
        var years_list = document.createElement('ul');
        years_list.className = 'year';
        // courses.schedule[YEAR][quartile][coursesIdx]
        courses.schedule.forEach(function(year) {
            var year_item = document.createElement('li');
            years_list.appendChild(year_item);
            var quartiles_list = document.createElement('ul');
            quartiles_list.className = 'quartile';
            year_item.appendChild(quartiles_list);

            // [year][QUARTILE][coursesIdx]
            year.forEach(function(quartile) {
                var quartile_item = document.createElement('li');
                quartiles_list.appendChild(quartile_item);
                var courses_list = document.createElement('ul');
                courses_list.className = 'courses';
                quartile_item.appendChild(courses_list);

                // [year][quartile][COURSESIDX]
                quartile.forEach(function(course) {
                    var course_item = document.createElement('li');
                    courses_list.appendChild(course_item);

                    // display course information
                    courseCallback(course_item, course);
                });
            });
        });
        return years_list;
    }

    return function() {
        // navigation
        var cmenu = document.getElementById("curriculum-menu");
        cmenu.appendChild(makeCoursesList(function(course_item, course) {
            // types: bc, major, bep
            course_item.dataset.type = course.type || 'major';
            course_item.title = course.name;

            var label = document.createElement('span');
            label.textContent = course.name;

            // display link for courses which have a description block
            if (course.id) {
                var link = document.createElement('a');
                link.href = '#/curriculum/' + course.id;
                link.appendChild(label);
                course_item.appendChild(link);
            } else {
                course_item.appendChild(label);
            }

            // HACK: this need to be fixed in CSS!
            course_item.addEventListener('click', function() {
                location.href = '#/curriculum/' + course.id;
            });
        }));

        var citems = document.getElementById('curriculum-items');
        courses.courses.forEach(function(course) {
            var header = document.createElement('h2');
            //header.id = 'course-' + course.id;
            header.id = '/curriculum/' + course.id;
            header.textContent = course.name + ' (' + course.when + ')';
            citems.appendChild(header);

            var text_block = document.createElement('p');
            text_block.textContent = course.description;
            citems.appendChild(text_block);

            var links_block = document.createElement('p');
            var back_to_top = document.createElement('a');
            back_to_top.href = '#/curriculum/menu';
            back_to_top.textContent = 'Back to table';
            links_block.appendChild(back_to_top);
            citems.appendChild(links_block);
        });
    };
});
